import {SET_CURRENT_USER} from '../actiontypes'

const DEFAULT_STATE = {
    user: {},
    isAuthenticated: false
}

const auth = (state = DEFAULT_STATE, action) => {
    switch(action.type){
        case SET_CURRENT_USER:
            return {
                // Check if there's a currrent user
                isAuthenticated: !!Object.keys(action.user).length,
                user: action.user
            }
        default:
            return state
    }
}

export default auth