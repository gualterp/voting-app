import serv from '../../services/serv'
import {SET_POLLS, SET_CURRENT_POLL} from '../actiontypes'
import {addError, removeError} from './error'

export const setPolls = polls => ({
    type: SET_POLLS,
    polls
})

export const setCurrentPoll = poll => ({
    type: SET_CURRENT_POLL,
    poll
})

export const getPolls = () => {
    return async dispatch => {
        try {
            const polls = await serv.call('get', 'poll');             
            dispatch(setPolls(polls));
            dispatch(removeError());
        } catch (err) {
            const error = err.response.data;
            dispatch(addError(error.message));
        }
    }
}

export const getUserPolls = () => {
    return async dispatch => {
        try {
            const userPolls = await serv.call('get', 'poll/user');
            dispatch(setPolls(userPolls));
            dispatch(removeError());
        } catch (err) {
            const error = err.response.data;
            dispatch(addError(error.message));
        }
    }
}

export const createPoll = data => {
    return async dispatch => {
        try {
            const poll = await serv.call('post', 'poll', data);
            dispatch(setCurrentPoll(poll));
            dispatch(removeError());
        } catch (err) {
            const error = err.response.data;
            dispatch(addError(error.message));
        }
    }
}

export const getCurrentPoll = id => {
    return async dispatch => {
        try {
            const poll = await serv.call('get', `poll/${id}`);
            dispatch(setCurrentPoll(poll));
            dispatch(removeError());
        } catch (err) {
            const error = err.response.data;
            dispatch(addError(error.message));
        }
    }
}

export const vote = (id, data) => {
    return async dispatch => {
        try {
            const poll = await serv.call('post', `poll/${id}`, data);
            dispatch(setCurrentPoll(poll));
            dispatch(removeError());
        } catch (err) {
            const error = err.response.data;
            dispatch(addError(error.message));
        }
    }
}