import {addError, removeError} from './error'
import {SET_CURRENT_USER} from '../actiontypes'
import serv from '../../services/serv'

export const setCurrentUser = user => ({
    type: SET_CURRENT_USER,
    user
})

export const setToken = token => {
    serv.setToken(token)
}

export const logout = () => {
    return dispatch => {
        localStorage.clear();
        serv.setToken(null);
        dispatch(setCurrentUser({}));
        dispatch(removeError());
    }
}

export const authUser = (path, data) => {
    return async dispatch => {
        try {
            const result = await serv.call('post', `auth/${path}`, data);
            const {token, ...user} = result;
            localStorage.setItem('jwtToken', token);
            serv.setToken(token);
            dispatch(setCurrentUser(user));
            dispatch(removeError());
        } catch (err) {
            const error = err.response.data;
            dispatch(addError(error.err.message));
        }
    }
}