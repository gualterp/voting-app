import {ADD_ERROR, REMOVE_ERROR} from '../actiontypes';

// Actions need their type and payload

export const addError = error => ({
    type: ADD_ERROR,
    error
});

export const removeError = () => ({
    type: REMOVE_ERROR
});