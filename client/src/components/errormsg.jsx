import React from 'react'
import {connect} from 'react-redux'

// Destructurin error from props
const ErrorMessage = ({error}) => (
    <div> {
    error.message && <div className='error'> {error.message} </div>
    } </div>
);

export default connect( store => ({ error: store.error }))(ErrorMessage)

