import React, {useEffect, useState} from 'react'
import {connect} from 'react-redux'

import {getPolls, getUserPolls, getCurrentPoll} from '../store/actions'

const Polls = (props) => {

    const {polls, auth, getPolls, getUserPolls} = props;

    useEffect(() => {        
        getPolls()
    }, [])

    // Change current URL to poll URL
    const handleSelect = id => {
        const {history} = props;
        history.push(`/poll/${id}`);
    }    
    
    const pollsList = polls.map(poll => (<li key={poll._id} onClick={() => handleSelect(poll._id)}>{poll.question}</li>));

    return (
        <div>
            {auth.isAuthenticated && (
                <div className='button-center'>
                    <button className='button' onClick={getPolls}>
                        All polls
                    </button>
                    <button className='button' onClick={getUserPolls}>
                        My polls
                    </button>
                </div>
            )}
            <ul className='poll-list'>{pollsList}</ul>
        </div>
    )    
}

export default connect(store => ({
    auth: store.auth,
    polls: store.polls
}), {
    getPolls, getUserPolls, getCurrentPoll
})(Polls);