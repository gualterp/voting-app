import React, {useState} from 'react'
import {connect} from 'react-redux'

import {authUser, logout} from '../store/actions'

const Auth = (props) => {

    const [loginDetails, setLoginDetails] = useState({
        username: '',
        password: ''
    })

    const handleSubmit = (e) => {
        e.preventDefault();
        const {username, password} = loginDetails;
        const {authType} = props       
        props.authUser(authType || 'login', {username, password});
    }

    return (
        <div>
            <form className='form' onSubmit={handleSubmit}>
                <label className='form-label' htmlFor="username">Username</label>
                <input className='form-input'type="text" value={loginDetails.username} name="username"
                       onChange={e => setLoginDetails(loginDetails => ({...loginDetails, username: e.target.value}))}/>
                <label className='form-label' htmlFor="password">Password</label>
                <input className='form-input' type="password" value={loginDetails.password} name="paswword"
                       onChange={e => setLoginDetails(loginDetails => ({...loginDetails, password: e.target.value}))}/>
                <div className='button-center'>
                    <button className='button' type="submit">Submit</button>
                </div>
            </form>
        </div>
    );
}

export default connect(() => ({}), {authUser, logout})(Auth);