import React from 'react'

import Poll from '../components/poll'
import ErrorMessage from '../components/errormsg'

const PollPage = ({match, getPoll}) => {  
    getPoll(match.params.id);

    return (
        <div>
            <ErrorMessage />
            <Poll />
        </div>
    )
}

export default PollPage