import React from 'react'

import Auth from '../components/auth'
import CreatePoll from '../components/createpoll'
import Poll from '../components/poll';
import Polls from '../components/polls'
import ErrorMessage from '../components/errormsg'

const TestPage = (props) => (
    <div>
        <h1>UI Test Page</h1>
        <div>
            <h2>Error</h2>
            <ErrorMessage />
        </div>
        <div>
            <h2>Auth</h2>
            <Auth />
        </div>
        <div>
            <h2>CreatePoll</h2>
            <CreatePoll />
        </div>
        <div>
            <h2>Polls</h2>
            <Polls {...props} />
        </div>
        <div>
            <h2>Poll</h2>
            <Poll />
        </div>
    </div>


    
)

export default TestPage
