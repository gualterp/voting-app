import React from 'react'

import Polls from '../components/polls';
import ErrorMessage from '../components/errormsg'

const HomePage = props => (
    <div>
        <ErrorMessage />
        <Polls {...props} />
    </div>
)

export default HomePage