import React from 'react'
import {Redirect} from 'react-router-dom'

import ErrorMessage from '../components/errormsg'
import CreatePoll from '../components/createpoll'

const CreatePollPage = ({isAuthenticated}) => {

    if(!isAuthenticated) return <Redirect to='/login'/>;

    return (
        <div>
            <ErrorMessage />
            <CreatePoll />
        </div>
    )
}

export default CreatePollPage
