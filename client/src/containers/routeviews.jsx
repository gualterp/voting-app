import React from 'react'
import {connect} from 'react-redux'
import {Switch, Route, withRouter} from 'react-router-dom'

import {getCurrentPoll} from '../store/actions'

import AuthPage from '../pages/authpage'
import TestPage from '../pages/testpage'
import HomePage from '../pages/homepage'
import PollPage from '../pages/pollpage'
import CreatePollPage from '../pages/createpollpage'

const RouteViews = ({auth, getCurrentPoll}) => (
    <div>
        <Switch>
            <Route exact path= '/' render= {props => <HomePage {...props} />}/>
            <Route exact path='/login' render={() => <AuthPage authType='login' isAuthenticated={auth.isAuthenticated} />}/>
            <Route exact path='/register' render={() => <AuthPage authType='register' isAuthenticated={auth.isAuthenticated} />}/>
            <Route exact path='/poll/new' render={() => <CreatePollPage isAuthenticated={auth.isAuthenticated} />}/>
            <Route exact path='/poll/:id' render={props => <PollPage getPoll={id => getCurrentPoll(id)} {...props} />} />
            <Route exact path='/test' render={() => <TestPage /> }/>
        </Switch>
    </div>
)

export default withRouter(connect(store => ({auth: store.auth}), {
    getCurrentPoll
})(RouteViews))