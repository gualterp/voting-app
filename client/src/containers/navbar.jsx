import React from 'react'
import {Link} from 'react-router-dom'
import {connect} from 'react-redux'

import {logout} from '../store/actions/auth'

const Navbar = ({auth, logout}) => (
    <nav className='navbar'>
        <div className='container'>
            <ul className='navbar-container'>
                <li>
                    <Link className='navbar-brand' to='/'> Home </Link>
                </li>            
                {!auth.isAuthenticated && (
                <div>
                    <li>
                        <Link className='navbar-item' to='/login' > Login </Link>
                    </li>
                    <li>
                        <Link className='navbar-item' to='/register' > Register </Link>
                    </li>  
                </div> 
                )}                          
                {auth.isAuthenticated && (
                <div>
                    <li>
                        <Link className='navbar-item' to="/poll/new"> Create Poll </Link>
                    </li>
                    <li>
                        <a className='navbar-item' href="/" onClick={logout}> Logout </a>
                    </li>
                </div>
                )}            
            </ul>
            {auth.isAuthenticated && (<p className='navbar-user'> Logged in as {auth.user.username} </p>)}
        </div>
    </nav>
)

export default connect(store => ({auth: store.auth}), {logout})(Navbar);