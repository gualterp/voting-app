// Dependencies
require('dotenv').config();
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

// Server configuration
const app = express();
const port = process.env.PORT || 4000;
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// Modules
const handle = require('./handlers');
const routes = require('./routes');

// Database connection
db = require('./models');

// Routing
app.get('/', (req, res) => res.send('Hello world'));
app.use('/auth', routes.auth);
app.use('/poll', routes.poll);

// Error handling
app.use(handle.errors);

// Start server
app.listen(port, () => console.log(`Server started on port ${port}`));