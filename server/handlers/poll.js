const db = require('../models');

exports.showPolls = async (req, res, next) => {
    try{
        //Get all polls
        const polls = await db.Poll.find().populate('user', ['username', 'id']);

        // Send response
        res.status(200).json(polls);

    } catch (err) {
        err.status = 400;
        next(err);
    }
}

exports.userPolls = async (req, res, next) => {
    try{
        // Get user polls
        const {id} = req.decoded; 
        const user = await db.User.findById(id).populate('polls');        

        // Send response
        res.status(200).json(user.polls);

    } catch {
        err.status = 400;
        next(err);
    }
}

exports.createPoll = async(req, res, next) => {
    try {
        // Get authenticated user
        const {id} = req.decoded;        
        const user = await db.User.findById(id);

        // Create new poll
        const {question, options} = req.body;
        const newPoll = await db.Poll.create({
            question, 
            user, 
            options : options.map(option => ({
                option, 
                votes: 0
            }))
        });

        // Update user's polls
        user.polls.push(newPoll._id)
        await user.save();

        // Send response
        res.status(200).json(
            {
                ...newPoll._doc,
                user: user._id
            });

    } catch (err) {
        err.status = 400;
        next(err);
    }
}

exports.getPoll = async (req, res, next) => {
    try {
        // ID comes in req parameters
        const {id} = req.params;

        // Find poll
        const poll = await db.Poll.findById(id).populate('user', ['username', 'id']);

        // Error handling
        if(!poll) throw new Error('Poll not found.');       

        // Send response
        res.status(200).json(poll);

    } catch (err) {
        err.status = 400;
        next(err);
    }
}

exports.vote = async (req, res, next) => {
    try {
        const {id: pollID} = req.params;
        const {id: userID} = req.decoded;

        // Get answer
        const {answer} = req.body;

        if(answer) {
            const poll = await db.Poll.findById(pollID);
            if(!poll) throw new Error('Poll not found.');

            // Add user vote
            const vote = poll.options.map(
                option => {
                    if(option.option === answer) {
                        return {
                            option: option.option,
                            _id: option._id,
                            votes: option.votes + 1
                        }
                    } else {
                        return option;
                    }
                }
            );

            // Verifiy if user already voted
            if(poll.voted.filter( user => user.toString() === userID).length <= 0) {
                poll.voted.push(userID);
                poll.options = vote;
                await poll.save();

                // Send response
                res.status(200).json(poll);

            } else {
                throw new Error('User already voted.');
            }
        } else {
            throw new Error('No answer provided.');
        }      

    } catch (err) {
        err.status = 400;
        next(err);
    }
}

exports.deletePoll = async (req, res, next) => {
    try {
        const {id: pollID} = req.params;
        const {id: userID} = req.decoded;

        // Get poll
        const poll = await db.Poll.findById(pollID);

        // Error handling
        if(!poll) throw new Error('Poll not found.');
        if(poll.user.toString() !== userID ) throw new Error('Unauthorized access.');

        // Delete poll
        await poll.remove();

        // Send response
        res.status(200).json(poll);

    } catch (err) {
        err.status = 400;
        next(err);
    }
}