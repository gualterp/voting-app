const jwt = require('jsonwebtoken');

const db = require('../models');

exports.register = async (req, res, next) => {
    try {
        const user = await db.User.create(req.body);
        // Deconstruct user
        const {id, username} = user;
        // Generate token
        const token = jwt.sign({id, username}, process.env.SECRET);
        res.status(200).json({id, username, token});
    } catch(err) {
        if(err.code === 11000) {
            err.message = "User already exists."
        }
        return next({status: 400, message: err.message});
    }
}

exports.login = async (req, res, next) => {
    try {
      // Get user
      const user = await db.User.findOne({
        username: req.body.username
      });     
      const { id, username } = user;

      // Check password
      const valid = await user.comparePassword(req.body.password);   
      if (valid) {       
        // Generate token
        const token = jwt.sign({id, username}, process.env.SECRET);
        // Send response
        return res.status(200).json({id,  username, token});
      } else {
        throw new Error();
      }
    } catch (err) {
        err.message = 'Invalid authentication.'
        next(err);
    }
  };