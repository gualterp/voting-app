module.exports = {
    ...require('./auth'),
    ...require('./poll')
};

// Managing errors
module.exports.errors = (err, req, res, next) => {
    return res.status(err.status || 400).json({
        success: false,
        err: {
            message: err.message || 'Something went wrong'
        }
    });
};