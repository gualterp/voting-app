const router = require('express').Router();

const handle = require('../handlers');
const auth = require('../middlewares/auth');

// Show all polls
router.route('/').get(handle.showPolls);

// Show user polls
router.route('/user').get(auth, handle.userPolls);

// Create new poll
router.route('/').post(auth, handle.createPoll);

// Get a specific poll
router.route('/:id').get(auth, handle.getPoll);

// Vote on a poll
router.route('/:id').post(auth, handle.vote);

// Delete poll
router.delete('/:id').post(auth, handle.deletePoll);

module.exports = router;